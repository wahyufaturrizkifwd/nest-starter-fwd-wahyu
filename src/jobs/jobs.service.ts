import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Votes } from 'src/shared/votes.enum';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateJobDto, CreateJobRo } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { Job } from './entities/job.entity';

@Injectable()
export class JobsService {
  constructor(
    @InjectRepository(Job) private jobRepository: Repository<Job>,
    @InjectRepository(User) private userRepository: Repository<User>,
  ) {}

  private toResponseObject(job: Job): CreateJobRo {
    const response: any = { ...job, user: job.user.toResponseObject(false) };

    if (response.upvotes) {
      response.upvotes = job.upvotes.length;
    }

    if (response.downvotes) {
      response.downvotes = job.downvotes.length;
    }

    return response;
  }

  private ensureOwnership(data: Job, userId: string) {
    if (data.user.id !== userId) {
      throw new HttpException('Incorect user', HttpStatus.UNAUTHORIZED);
    }
  }

  private async vote(job: Job, user: User, vote: Votes) {
    const opposite = vote === Votes.UP ? Votes.DOWN : Votes.UP;

    if (
      job[opposite].filter((filtering) => filtering.id === user.id).length >
        0 ||
      job[vote].filter((filtering) => filtering.id === user.id).length > 0
    ) {
      job[opposite] = job[opposite].filter(
        (filtering) => filtering.id !== user.id,
      );
      job[vote] = job[vote].filter((filtering) => filtering.id !== user.id);

      await this.jobRepository.save(job);
    } else if (
      job[vote].filter((filtering) => filtering.id === user.id).length < 1
    ) {
      job[vote].push(user);
      await this.jobRepository.save(job);
    } else {
      throw new HttpException('Unable to cast vote', HttpStatus.BAD_REQUEST);
    }

    return job;
  }

  async create(id: string, createJobDto: CreateJobDto): Promise<CreateJobRo> {
    const user = await this.userRepository.findOne({ where: { id } });

    const job = await this.jobRepository.create({
      ...createJobDto,
      user: user,
    });
    await this.jobRepository.save(job);

    return this.toResponseObject(job);
  }

  async findAll(page: number = 1, newest?: boolean): Promise<CreateJobRo[]> {
    const job = await this.jobRepository.find({
      relations: ['user', 'upvotes', 'downvotes', 'comments'],
      take: 10,
      skip: 2 * (page - 1),
      order: newest && {createdAt: 'DESC'},
    });

    if (!job) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }

    return job.map((data) => this.toResponseObject(data));
  }

  async findOne(id: string): Promise<CreateJobRo> {
    const job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user', 'upvotes', 'downvotes', 'comments'],
    });

    if (!job) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    return this.toResponseObject(job);
  }

  async update(
    id: string,
    userId: string,
    updateJobDto: Partial<UpdateJobDto>,
  ) {
    let job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user'],
    });

    if (!job) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    this.ensureOwnership(job, userId);
    await this.jobRepository.update({ id }, updateJobDto);
    job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user', 'comments'],
    });
    return this.toResponseObject(job);
  }

  async remove(id: string, userId: string) {
    const job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user', 'comments'],
    });
    if (!job) {
      throw new HttpException('Not found', HttpStatus.NOT_FOUND);
    }
    this.ensureOwnership(job, userId);
    await this.jobRepository.delete({ id });
    return { deleted: true };
  }

  async bookmarkJob(id: string, userId: string) {
    const job = await this.jobRepository.findOne({ where: { id } });
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['bookmarks'],
    });

    if (
      user.bookmarks.length < 1 ||
      user.bookmarks.filter((filtering) => filtering.id === job.id).length < 1
    ) {
      user.bookmarks.push(job);
      await this.userRepository.save(user);
    } else {
      throw new HttpException('Job already bookmarked', HttpStatus.BAD_REQUEST);
    }

    return user.toResponseObject(false);
  }

  async unbookmarkJob(id: string, userId: string) {
    const job = await this.jobRepository.findOne({ where: { id } });
    const user = await this.userRepository.findOne({
      where: { id: userId },
      relations: ['bookmarks'],
    });

    if (
      user.bookmarks.filter((filtering) => filtering.id === job.id).length > 0
    ) {
      user.bookmarks = user.bookmarks.filter(
        (filtering) => filtering.id !== job.id,
      );
      await this.userRepository.save(user);
    } else {
      throw new HttpException('Job already unbookmark', HttpStatus.BAD_REQUEST);
    }

    return user.toResponseObject(false);
  }

  async upvoteJob(id: string, userId: string) {
    let job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user', 'upvotes', 'downvotes', 'comments'],
    });

    const user = await this.userRepository.findOne({ where: { id: userId } });

    job = await this.vote(job, user, Votes.UP);
    return this.toResponseObject(job);
  }

  async downvoteJob(id: string, userId: string) {
    let job = await this.jobRepository.findOne({
      where: { id },
      relations: ['user', 'upvotes', 'downvotes', 'comments'],
    });
    const user = await this.userRepository.findOne({ where: { id: userId } });

    job = await this.vote(job, user, Votes.DOWN);
    return this.toResponseObject(job);
  }
}
