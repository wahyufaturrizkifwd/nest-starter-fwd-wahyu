import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty } from 'class-validator';
import { LoginUserRo } from 'src/users/dto/login-user.dto.';
import { CreateJobDto } from './create-job.dto';

export class UpdateJobDto extends PartialType(CreateJobDto) {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  desc: string;

  @IsNotEmpty()
  isActive: boolean;

  @IsNotEmpty()
  createdAt: Date;

  @IsNotEmpty()
  updatedAt: Date;

  @IsNotEmpty()
  deletedAt: Date;
}

export class UpdateJobRo {
  id?: string;
  name?: string;
  desc?: string;
  isActive?: boolean;
  upvotes?: number;
  downvotes?: number;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  user?: LoginUserRo;
}
