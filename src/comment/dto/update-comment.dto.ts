import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsString } from 'class-validator';
import { LoginUserRo } from 'src/users/dto/login-user.dto.';
import { CreateCommentDto } from './create-comment.dto';

export class UpdateCommentDto extends PartialType(CreateCommentDto) {
  @IsNotEmpty()
  @IsString()
  comment: string;
}

export class UpdateCommentRo {
  id?: string;
  comment?: string;
  user?: LoginUserRo;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
