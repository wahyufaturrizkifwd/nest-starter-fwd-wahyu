import { IsNotEmpty, IsString } from 'class-validator';
import { LoginUserRo } from 'src/users/dto/login-user.dto.';

export class CreateCommentDto {
  @IsNotEmpty()
  @IsString()
  comment: string;
}

export class CreateCommentRo {
  id?: string;
  comment?: string;
  user?: LoginUserRo;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
